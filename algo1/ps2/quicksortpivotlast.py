# input (Array, start, end)
def partition_pivotLast(A,l,r):
	# NOTE: LAST = r-1, != r
	p = A[r-1]
	i = l
	# r = len(A)
	# !!! WARNING !!!
	# DO NOT INCLUDE A[r]
	for j in range(l,r-1):
		if A[j] < p:
			# swap A[i] <-> A[j]
			A[i], A[j] = A[j], A[i]
			i = i + 1
	A[r-1], A[i] = A[i], A[r-1]
	return i

def quicksort_pivotLast(A, left, right):
	if right > left:

		fixIndex = partition_pivotLast(A,left,right)

		quicksort_pivotLast(A,left,fixIndex)
		
		quicksort_pivotLast(A,fixIndex+1,right)

if __name__ == '__main__':
	testingArray = [1,3,5,2,4,11,15,7,17,6,9]
	quicksort_pivotLast(testingArray,0,len(testingArray))
	print(testingArray)