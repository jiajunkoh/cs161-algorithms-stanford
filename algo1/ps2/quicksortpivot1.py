# input (Array, start, end)
def partition_pivot1(A,l,r):
	p = A[l]
	i = l+1
	# r = len(A)
	# !!! WARNING !!!
	# DO NOT INCLUDE A[r]
	for j in range(l+1,r):
		if A[j] < p:
			# swap A[i] <-> A[j]
			A[i], A[j] = A[j], A[i]
			i = i + 1
	A[l], A[i-1] = A[i-1], A[l]
	return i-1

def quicksort_pivot1(A, left, right):
	if right > left:
		fixIndex = partition_pivot1(A,left,right)

		quicksort_pivot1(A,left,fixIndex)
		
		quicksort_pivot1(A,fixIndex+1,right)

if __name__ == '__main__':
	testingArray = [1,3,5,2,4,11,15,7,17]
	quicksort_pivot1(testingArray,0,len(testingArray))
	print(testingArray)