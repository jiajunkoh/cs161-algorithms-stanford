def quicksort(A,left,right):
	if right > left:
		fixIndex = partition(A,left,right)

		quicksort(A,left,fixIndex)
		
		quicksort(A,fixIndex+1,right)

def partition(A,l,r):
	length = r-l;
	if length % 2 == 0:
		middleIndex = l + length//2 - 1
	else:
		middleIndex = l + length//2

	# unallowed memory allocation
	# could have just used comparison to find
	# median of [A[l],...,A[mid],....,A[r]]
	temp = [A[l], A[middleIndex], A[r-1]]
	temp.sort()
	medianIndex = A.index(temp[1])
	# swap A[0] <-> A[medianIndex]
	# to ease up the procedure
	A[l], A[medianIndex] = A[medianIndex], A[l]
	
	p = A[l]
	i = l+1
	# r = len(A)
	# !!! WARNING !!!
	# DO NOT INCLUDE A[r]
	for j in range(l+1,r):
		if A[j] < p:
			# swap A[i] <-> A[j]
			A[i], A[j] = A[j], A[i]
			i = i + 1
	A[l], A[i-1] = A[i-1], A[l]
	return i-1

if __name__ == '__main__':
	test = [1,3,4,2,7,8,5,10,11,16]
	quicksort(test,0,len(test))
	test2 = [10,9,8,7,6,5,4,3,2,1,0]
	quicksort(test2,0,len(test2))
	print(test)
	print(test2)
