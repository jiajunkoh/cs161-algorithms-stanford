def readGraph(filename):
	file = open(filename)
	text = file.read()
	file.close()
	
	text = text.split('\n')
	
	Vptr = {}
	VptrReverse = {}
	V = []

	for i in range(1,875715):
		V.append(i)
		VptrReverse[i] = []
		Vptr[i] = []

	for i in range(len(text)):
		print(i)
		edge = text[i].split(' ')
		Vptr[int(edge[0])].append(int(edge[1]))
		VptrReverse[int(edge[1])].append(int(edge[0]))
		
	return V,Vptr,VptrReverse

