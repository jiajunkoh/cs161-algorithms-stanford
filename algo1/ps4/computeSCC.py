# note that Strongly Connected Components (SCC) 
# - only exists in directed graph
# - definition : there exist a path from every u to every v, and vice versa (v to u)
# - use DFS
# - compute the largest number of SCC Graph

#import sys
#sys.settrace
#import resource
#resource.setrlimit(resource.RLIMIT_STACK, (2 ** 28, 2 ** 29))

from readGraph import readGraph

print('READING GRAPH')
V, Vadj, VadjR = readGraph('SCC.txt')
'''
V = [1,2,3,4,5,6,7,8,9,10,11]

Vadj = {}
Vadj[1] = [2]
Vadj[2] = [3, 4]
Vadj[3] = [1]
Vadj[4] = [5]
Vadj[5] = [6]
Vadj[6] = [4,7]
Vadj[7] = [8]
Vadj[8] = [9]
Vadj[9] = [10]
Vadj[10] = [7, 11]
Vadj[11] = []

VadjR = {}
VadjR[1] = [3]
VadjR[2] = [1]
VadjR[3] = [2]
VadjR[4] = [2,6]
VadjR[5] = [4]
VadjR[6] = [5]
VadjR[7] = [6,10]
VadjR[8] = [7]
VadjR[9] = [8]
VadjR[10] = [9]
VadjR[11] = [10]
'''

'''V = ['A','B','C','D','E','F','G','H','I','J','K']

Vadj = {}
Vadj['A'] = ['B']
Vadj['B'] = ['C', 'D']
Vadj['C'] = ['A']
Vadj['D'] = ['E']
Vadj['E'] = ['F']
Vadj['F'] = ['D','G']
Vadj['G'] = ['H']
Vadj['H'] = ['I']
Vadj['I'] = ['J']
Vadj['J'] = ['G', 'K']
Vadj['K'] = []

VadjR = {}
VadjR['A'] = ['C']
VadjR['B'] = ['A']
VadjR['C'] = ['B']
VadjR['D'] = ['B','F']
VadjR['E'] = ['D']
VadjR['F'] = ['E']
VadjR['G'] = ['F','J']
VadjR['H'] = ['G']
VadjR['I'] = ['H']
VadjR['J'] = ['I']
VadjR['K'] = ['J']'''


print('DONE READING GRAPH')

explored = {}
f = {} # finishing time
leader = {}
t = 0
s = None

def DFSLoopReverse(V, VadjR):
	'''
	get the magical ordering of finishing time
	'''
	global t
	t = 0
	for i in range(len(V)):
		if V[i] not in explored:
			DFSReverse(V, VadjR, V[i])

#def DFSReverseIterative(V, VadjR, i):





def DFSReverse(V, VadjR, i):
	explored[i] = 1
	# for each arc (i,j) in G
	#	if j not explored
	#		DFS(G,j)
	if i in VadjR:
		for j in range(len(VadjR[i])):
			node = VadjR[i][j]
			if node not in explored:
				DFSReverse(V, VadjR, node)
	global t
	t = t + 1
	f[i] = t

def DFSLoop(V, Vadj):
	'''
	explore according finishing time
	'''
	global s
	s = None
	for i in range(len(finishing)):
		global length
		length = 0
		if finishing[i] not in explored:
			DFS(V, Vadj, finishing[i])
		if length > 200:
			print(length)

def DFS(V, Vadj, i):
	global length
	length = length + 1
	explored[i] = 1
	global s
	leader[i] = s
	if i in Vadj:
		for j in range(len(Vadj[i])):
			node = Vadj[i][j]
			if node not in explored:
				DFS(V, Vadj, node)

finishing = []

def main():
	DFSLoopReverse(V, VadjR)
	#print(f)
	global f
	global finishing
	for i in f:
		finishing.append(i)
	finishing.reverse()
	#print(finishing)
	global explored
	explored = {}
	DFSLoop(V,Vadj)

import sys
sys.setrecursionlimit(2 ** 20)
# !!! THIS EFFECTIVELY INCREASES OUR STACK SIZE
import threading
threading.stack_size(67108864)
thread = threading.Thread(target = main)
thread.start();







