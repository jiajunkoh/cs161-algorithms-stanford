def readGraph(filename):
	file = open(filename)
	text = file.read()
	file.close()

	text = text.split('\n')

	Wptr = {}
	adj = {}
	for i in range(len(text)):
		line = text[i].split('	')
		adj[line[0]] = []
		for j in range(1,len(line)):
			try:
				E = line[j].split(',')
				Wptr[line[0] + ', ' + E[0]] = int(E[1])
				adj[line[0]].append(E[0])
			except:
				print('error')
	return adj, Wptr
