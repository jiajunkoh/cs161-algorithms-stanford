from readGraph import readGraph

V = []
for i in range(1,201):
	V.append(str(i))

adj, weight = readGraph('dijkstraData.txt')


d = {}
for vertex in V:
	if vertex == 'A':
		d[vertex] = 0
	else:
		d[vertex] = 9999999

# predecessor of best path
bestPath = {}
bestPath['1'] = None


def dijkstra(s):
	shortestPath = []
	Q = {}
	for i in V:
		if i == s:
			Q[i] = 0
		else:
			Q[i] = 9999999
	shortestDistance = Q.copy()
	while Q:
		smallestQ = 99999
		smallestQVertex = None
		for i in Q:
			if shortestDistance[i] < smallestQ:
				smallestQ = shortestDistance[i]
				smallestQVertex = i
		del Q[smallestQVertex]
		shortestPath.append(smallestQVertex)
		for v in adj[smallestQVertex]:
			# relaxation
			key = smallestQVertex + ", " + v
			if shortestDistance[v] > \
			shortestDistance[smallestQVertex] + weight[key]:
				shortestDistance[v] = shortestDistance[smallestQVertex] + weight[key]

	print(shortestDistance)
	print(shortestPath)

dijkstra('1')