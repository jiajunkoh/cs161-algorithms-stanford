def SortAndCount(A,n):
	if n == 1:
		return A,0
	else:
		A1 = A[:n//2]
		(B,x) = SortAndCount(A1,len(A1))

		A2 = A[n//2:]
		(C,y) = SortAndCount(A2,len(A2))

		(D,z) = MergeAndCountSplitInv(B,C)

		return (D,x+y+z)

def MergeAndCountSplitInv(B,C):
	sortArray = []
	invCounter = 0
	while(len(B) > 0 and len(C) > 0):
		if(B[0] < C[0]):
			sortArray.append(B[0])
			B = B[1:]
		else:
			sortArray.append(C[0])
			invCounter = invCounter + len(B)
			C = C[1:]

	# copy the remaining entries into the array
	while (len(B) > 0 or len(C) > 0):
		if len(B) > 0:
			sortArray.append(B[0])
			B = B[1:]
		else:
			sortArray.append(C[0])
			C = C[1:]

	print(invCounter)
	return sortArray,invCounter

if __name__ == '__main__':
	filename = "IntegerArray.txt"
	integerArray = []
	with open(filename,'r') as f:
		for line in f:
			integerArray.append(int(line))	
	print(SortAndCount(integerArray,len(integerArray))[1])
