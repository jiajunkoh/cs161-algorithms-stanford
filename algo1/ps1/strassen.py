import numpy as np
import time
# n x n matrix
n = 128
mat1 = np.random.random(n**2)*100
mat1 = mat1.reshape(n,n)
mat2 = np.random.random(n**2)*100
mat2 = mat2.reshape(n,n)

def bruteForce(mat1, mat2, size):
	#result = {}
	#for i in range(size):
	#	for j in range(size):
	#		result[i,j] = 0
	#		for k in range(size):
	#			result[i,j] = result[i,j] + mat1[i,k]*mat2[k,j]
	#return result
	result = np.zeros([size, size])
	for i in range(size):
		for j in range(size):
			for k in range(size):
				result[i,j] += mat1[i,k]*mat2[k,j]
	return result

def strassen(mat1, mat2, size):
	if size <= 4:
		return bruteForce(mat1, mat2, size)
	
	midway = size//2
	
	'''if midway % 2 == 0:
					A = mat1[0:midway, 0:midway]
					B = mat1[0:midway, midway:]
					E = mat2[0:midway, 0:midway]
					F = mat2[0:midway, midway:]
					size2 = size//2
				else:
					# pad zeros to row and column if odd dimension
					A = np.zeros([midway+1, midway+1])
					B = np.zeros([midway+1, midway+1])
					A[0:midway, 0:midway] = mat1[0:midway, 0:midway]
					B[0:midway, 0:midway] = mat1[0:midway, midway:]
					E = np.zeros([midway+1, midway+1])
					F = np.zeros([midway+1, midway+2])
					E[0:midway, 0:midway] = mat2[0:midway, 0:midway]
					F[0:midway, 0:midway] = mat2[0:midway, midway:]
					size2 = size//2 + 1'''

	A = mat1[0:midway, 0:midway]
	B = mat1[0:midway, midway:]
	E = mat2[0:midway, 0:midway]
	F = mat2[0:midway, midway:]
	C = mat1[midway:, 0:midway]
	D = mat1[midway:, midway:]
	G = mat2[midway:, 0:midway]
	H = mat2[midway:, midway:]
	size2 = midway

	P1 = strassen(A,F-H,size2)
	P2 = strassen(A+B,H,size2)
	P3 = strassen(C+D,E,size2)
	P4 = strassen(D,G-E,size2)
	P5 = strassen(A+D,E+H,size2)
	P6 = strassen(B-D,G+H,size2)
	P7 = strassen(A-C,E+F,size2)

	result = np.zeros([size,size])
	result[0:midway, 0:midway] = P5+P4-P2+P6
	result[0:midway, midway:] = P1+P2
	result[midway:, 0:midway] = P3+P4
	result[midway:, midway:] = P1+P5-P3-P7
	return result



#print('mat1:')
#print(mat1)
#print('mat2:')
#print(mat2)
#print()
print('BruteForce: ')
time1 = time.time()
result = bruteForce(mat1, mat2, n)
#print(result)
print('Time elapsed: {0}'.format(time.time() - time1))
print()
print('Strassen')
time2 = time.time()
result2 = strassen(mat1,mat2,n)
#print(result2)
print('Time elapsed: {0}'.format(time.time() - time2))
print()
print('Built-in:')
time3 = time.time()
result3 = mat1.dot(mat2)
#print(result3)
print('Time elapsed: {0}'.format(time.time() - time3))