def getDigit(x):
	if x == 0:
		return 0
	else:
		return 1 + getDigit(x//10)

def simpleMultiplication(x,y):
	if x == 0 or y == 0:
		return 0
	elif x == 1:
		return y
	else:
		return y + simpleMultiplication(x-1,y)

def karatsuba(x,y):
	n = getDigit(x)
	if n <= 1:
		return simpleMultiplication(x,y)
	else:
		half_n = n//2
		a = x//(10**half_n)
		b = x%10**half_n
		c = y//(10**half_n)
		d = y%10**half_n
		return karatsuba(a,c)*(10**n) + (karatsuba(a,d)+karatsuba(b,c))*(10**half_n) + karatsuba(b,d)

import time
import sys
sys.setrecursionlimit(99999)

print('simpleMultiplication(5678,123456): ')
t1 = time.time()
print(simpleMultiplication(5678,123456))
print('time taken: {0}'.format(time.time() - t1))
print()
print('karatsuba(5678,123456): ')
t1 = time.time()
print(karatsuba(5678,123456))
print('time taken: {0}'.format(time.time() - t1))