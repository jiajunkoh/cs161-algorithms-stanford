# 1D version O(nlogn) running time
def closestpair_1d(A):
	A.sort()
	minDist = A[1] - A[0]
	minIndex = 0
	for i in range(1,len(A)-1):
		if A[i+1] - A[i] < minDist:
			minDist = A[i+1] - A[i]
			minIndex = i
	return (A[minIndex],A[minIndex+1])

def test1D(A):
	print(closestpair_1d(A))

# 2D version goal: O(nlogn) running time
# p = [x1,y1]; q = [x2,y2]
def dist(p,q):
	return ((p[0]-q[0])**2+(p[1]-q[1])**2)**0.5

def bruteforce2D(A):
	minDist = 99999
	min1 = 0
	min2 = 0
	for i in range(len(A)):
		for j in range(i+1,len(A)):
			if dist(A[i],A[j]) < minDist:
				minDist = dist(A[i],A[j])
				min1 = i
				min2 = j
	return (A[min1],A[min2])

def distTuple(A):
	return ((A[0][0]-A[1][0])**2 + (A[0][1]-A[1][1])**2)**0.5

def closestPair_2d(Px,Py):
	# base case, use bruteforce to find
	if(len(Px) <= 4):
		Qx = bruteforce2D(Px)
		Qy = bruteforce2D(Py)
		if(distTuple(Qx) > distTuple(Qy)):
			return Qy
		else:
			return Qx
	else:		
		Qx = Px[:len(Px)//2];
		Qy = Py[:len(Py)//2];
		# R = right half of P
		Rx = Px[len(Px)//2:];
		Ry = Py[len(Py)//2:];
		(p1,q1) = closestPair_2d(Qx,Qy)
		(p2,q2) = closestPair_2d(Rx,Ry)
		deltaDist = min([dist(p1,q1),dist(p2,q2)])
		(p3,q3) = closestSplitPair(Px,Py,deltaDist)
		if dist(p3,q3) <= deltaDist:
			return (p3,q3)
		else:
			if deltaDist == (p1,q1):
				return (p1,q1)
			else:
				return (p2,q2)

def closestSplitPair(Px,Py,deltaDist):
	x_bar = Px[len(Px)//2][0]
	Sy = []
	for i in Py:
		if i[0] >= x_bar - deltaDist and i[0] <= x_bar + deltaDist:
			Sy.append(i)
	best = deltaDist
	bestpair = [99999999,99999999],[-99999999,-99999999]
	for i in range(len(Sy)-7):
		for j in range(7):
			p = Sy[i]
			q = Sy[i+j]
			if dist(p,q) < best:
				bestpair = (p,q)
				best = deltaDist
	return bestpair



if __name__ == '__main__':
	# test1D = [99,93,91,95,97,100]
	test2D = [[-10,0],[0,-3],[-4,0],[10,10],[7,8],[7,0]]
	print(bruteforce2D(test2D))
	Px = sorted(test2D)
	# sort test2D by y-coordinates
	Py = sorted(test2D, key=lambda x: x[1])
	print(closestPair_2d(Px,Py))