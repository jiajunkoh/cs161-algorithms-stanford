def mergesort(A):
	if len(A) == 1:
		return A
	else:
		middle = len(A)//2
		firstHalf = mergesort(A[:middle])
		secondHalf = mergesort(A[middle:])
		return merge(firstHalf,secondHalf)

def merge(A,B):
	i = 0
	j = 0
	C = []
	for k in range(len(A)+len(B)):
		if(i < len(A) and j < len(B)):
			if(A[i] < B[j]):
				C.append(A[i])
				i = i + 1
			else:
				C.append(B[j])
				j = j + 1
		elif(i >= len(A)):
			C.append(B[j])
			j = j + 1
		else:
			C.append(A[i])
			i = i + 1
	return C

if __name__ == '__main__':
	test = [1,2,3,4,6,5]
	print(mergesort(test))
	test2 = [6,5,4,3,2,1]
	print(mergesort(test2))