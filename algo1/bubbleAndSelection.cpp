#include <iostream>

using namespace std;

void bubbleSort(int* A, int size){
  int sortingFlag = 0;
  while(!sortingFlag){
    sortingFlag = 1;
    for(int i = 0; i < size-1; i++){
      if(A[i] > A[i+1]){
        // swap A[i] <-> A[i+1]
        int temp = A[i];
        A[i] = A[i+1];
        A[i+1] = temp;
        sortingFlag = 0;
      }
    }
  }
}

void selectionSort(int* A, int size){
  // search for minimum
  // swap places the leftmost unsorted <-> minimum
  for(int i = 0; i < size; i++){
    int min = A[i];
    int index = i;
    for(int j = i+1; j < size; j++){
      if(A[j] < min){
        min = A[j];
        index = j;
      }
    }
    if(index != i){
      // swap A[i] <-> A[index]
      int temp = A[i];
      A[i] = A[index];
      A[index] = temp;
    }
  }
}

void printArray(int*,int);

int main(){
  int array[] = {5,7,6,1,4,2,3};
  bubbleSort(array,7);
  printArray(array,7);
  int array2[] = {11,13,12,15,17,16,19};
  selectionSort(array2,7);
  printArray(array2,7);
}

void printArray(int* array, int size){
  for(int i = 0; i < size; i++){
    cout << array[i] << " ";
  }
  cout << endl;
}
