import random
V = ['v1','v2','v3','v4']
E = ['e1','e2','e3','e4','e5']
Vptr = [['v2','v3','v4'],['v1','v3'],['v1','v2','v4'],['v1','v3']]
Eptr = [['v1','v2'],['v2','v3'],['v3','v4'],['v1','v4'],['v1','v3']]


def randomContractionAlgo():
	contractionSequence = []
	while len(V) > 2:
		# pick a remaining edge (u,v) uniformly at random
		removeEdgeIndex = random.randint(0,len(E)-1)
		contractionSequence.append(E[removeEdgeIndex])

		# merge (or contract) u and v into a single vertex
		mergeVertex, toBeMerged = Eptr[removeEdgeIndex]
		mergeVertexIndex = V.index(mergeVertex)
		toBeMergedIndex = V.index(toBeMerged)

		del Vptr[mergeVertexIndex][Vptr[mergeVertexIndex].index(toBeMerged)]
		del Vptr[toBeMergedIndex][Vptr[toBeMergedIndex].index(mergeVertex)]
		
		Vptr[toBeMergedIndex] = Vptr[mergeVertexIndex] + Vptr[toBeMergedIndex]

		del Vptr[mergeVertexIndex]

		for eachList in range(len(Vptr)):
			if mergeVertex in Vptr[eachList]:
				for element in range(len(Vptr[eachList])):
					if Vptr[eachList][element] == mergeVertex:
						Vptr[eachList][element] = toBeMerged

		for eachList in range(len(Eptr)):
			if mergeVertex in Eptr[eachList]:
				for element in range(len(Eptr[eachList])):
					if Eptr[eachList][element] == mergeVertex:
						Eptr[eachList][element] = toBeMerged

		del V[mergeVertexIndex]
		del E[removeEdgeIndex]
		
		del Eptr[removeEdgeIndex]
		
		# remove self-loops
		i = 0
		while i < len(Eptr):
			if Eptr[i][0] == Eptr[i][1]:
				del Eptr[i]
				del E[i]
			else:
				i = i + 1

		for eachList in range(len(Vptr)):
			if V[eachList] in Vptr[eachList]:
				j = 0
				while j < len(Vptr[eachList]):
					if Vptr[eachList][j] == V[eachList]:
						del Vptr[eachList][j]
					else:
						j = j + 1

	print(V)
	print(Vptr)
	print(E)
	print(Eptr)
	print(contractionSequence)
	return V

randomContractionAlgo()

