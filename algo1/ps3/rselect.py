import random

def partition_pivot1(A,l,r):
	p = A[l]
	i = l+1
	# r = len(A)
	# !!! WARNING !!!
	# DO NOT INCLUDE A[r]
	for j in range(l+1,r):
		if A[j] < p:
			# swap A[i] <-> A[j]
			A[i], A[j] = A[j], A[i]
			i = i + 1
	A[l], A[i-1] = A[i-1], A[l]
	return i-1

def Rselect_randpivot(A,i):
	if len(A) == 1:
		return A[0]
	pivotIndex = random.randint(0,len(A)-1)
	A[0], A[pivotIndex] = A[pivotIndex], A[0]
	j = partition_pivot1(A,0,len(A))
	if j == i:
		return A[j]
	elif j > i:
		return Rselect_randpivot(A[:j],i)
	else:
		return Rselect_randpivot(A[j+1:],i-j-1)

def Rselect_medianpivot(A,i):
	length = len(A)
	if length == 1:
		return A[0]
	if length % 2 == 0:
		middleIndex = length//2 - 1
	else:
		middleIndex = length//2
	
	# get median as pivot
	if (A[0] - A[middleIndex])*(A[length-1] - A[0]) >= 0:
		pivotIndex = 0
	elif (A[middleIndex] - A[0])*(A[length-1] - A[middleIndex]) >= 0:
		pivotIndex = middleIndex
	else:
		pivotIndex = length-1

	A[0], A[pivotIndex] = A[pivotIndex], A[0]
	j = partition_pivot1(A,0,length)
	if j == i:
		return A[j]
	elif j > i:
		return Rselect_medianpivot(A[:j],i)
	else:
		return Rselect_medianpivot(A[j+1:],i-j-1)


if __name__ == '__main__':
	A = [300,100,200,400,600,500,700,1000,900,800]
	# index starts by 0 
	print(Rselect_medianpivot(A,8))
