V = ['v1','v2','v3','v4']

# adjacency matrix representation
# adj(M) =		v1	v2	v3	v4
#			v1	0	1	1	1
#			v2	1	0	1	0
#			v3	1	1	0	1
#			v4	1	0	1	0

adjacency_matrix = [[0,1,1,1],[1,0,1,0],[1,1,0,1],[1,0,1,0]]

def test_adjMat(V,m):
	length = len(m)
	for i in range(length):
		for j in range(length):
			if m[i][j] >= 1:
				print("{} -> {}".format(V[i],V[j]))

test_adjMat(V,adjacency_matrix)

print("\n===============\n")

# adjacency lists representation
# Vptr =	v1:	v2	v3	v4 
#			v2:	v1	v3
#			v3:	v1	v2	v4
#			v4:	v1	v3
Vptr = [['v2','v3','v4'],['v1','v3'],['v1','v2','v4'],['v1','v3']]

def test_adjList(V,mptr):
	for i in range(len(V)):
		for j in range(len(mptr[i])):
			print("{} -> {}".format(V[i],mptr[i][j]))

test_adjList(V,Vptr)