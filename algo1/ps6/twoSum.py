from readData import readData
from readData import readDataToDic

def count2Sum(numlist):
	# a list of num
	# find number of distinct sums of two numbers
	dic = {}
	for i in range(len(numlist)):
		print(i)
		j = len(numlist) - 1
		cont = True
		while cont and j > i:
			twosum = numlist[i] + numlist[j]
			if(abs(twosum) > 10000):
				if(numlist[i] < 0 and numlist[j] > 0 and twosum > 0):
					j = j - 1
				else:
					cont = False
			else:
				# append
				dic[twosum] = 1
				j = j - 1
	print(dic)
	return len(dic)


def count2SumWithDic(dic, dickeys):
	result = {}
	for i in range(-10000,10001):
		result[i] = 0
	resultcopy = result.copy()
	
	for i in range(len(dickeys)):
		print("iteration: {0} remaining keys: {1}".format(i, len(result)))
		# targetMax = 10000 - dickeys
		# targetMin = -10000 - dickeys	
		for key in result.keys():
			if key - dickeys[i] in dic:
				del resultcopy[key]
		result = resultcopy.copy()
	return result

data = readData('algo1-programming_prob-2sum.txt')
dic = readDataToDic('algo1-programming_prob-2sum.txt')
#num = count2Sum(data)
#print('THE DISTINCT COUNT: ')
#print(num)

dic = count2SumWithDic(dic, data)