def readData(filename):
	file = open(filename)
	text = file.read()
	file.close()

	text = text.split('\n')

	arr = []

	for i in range(len(text)):
		arr.append(int(text[i]))

	# print(arr)
	return arr

# Continuous Median
# 1. Make two heaps (maxHeap, minHeap)
# 2. Add num into heap appropriately
# 3. Rebalance
# 4. Get Median

def parent(i):
	return (i+1)//2 - 1

def leftChild(i):
	return 2*(i+1) - 1

def rightChild(i):
	return 2*(i+1)

def addNumber(val, maxHeap, minHeap):
	if len(maxHeap) == 0 or maxHeap[0] > val:
		maxHeapInsert(val, maxHeap)
	else:
		minHeapInsert(val, minHeap)

def maxHeapInsert(val, maxHeap):
	maxHeap.append(val)
	ind = len(maxHeap) - 1
	while maxHeap[parent(ind)] < maxHeap[ind] and ind > 0:
		maxHeap[parent(ind)], maxHeap[ind] = maxHeap[ind], maxHeap[parent(ind)]
		ind = parent(ind)
	
def minHeapInsert(val, minHeap):
	minHeap.append(val)
	ind = len(minHeap) - 1
	while minHeap[parent(ind)] > minHeap[ind] and ind > 0:
		minHeap[parent(ind)], minHeap[ind] = minHeap[ind], minHeap[parent(ind)]
		ind = parent(ind)
	
def rebalance(maxHeap, minHeap):
	if len(maxHeap) - len(minHeap) >= 2:
		# lower heap larger than higher
		# pop maxHeap largest root and put into minHeap
		minHeapInsert(maxHeap[0], minHeap)
		# O(log n) EXTRACT_MAX()
		extractMax(maxHeap)

	if len(minHeap) - len(maxHeap) >= 2:
		# higher heap larger than lower
		maxHeapInsert(minHeap[0], maxHeap)
		extractMin(minHeap)

def modifiedMaxHeapify(A,i,length):
	if leftChild(i) < length:
		if rightChild(i) >= length:
			# only left child exists
			if A[leftChild(i)] > A[i]:
				A[leftChild(i)], A[i] = A[i], A[leftChild(i)]
		else:
			# have both right and left
			if A[leftChild(i)] > A[i] and A[leftChild(i)] > A[rightChild(i)]:
				# swap left with i
				A[leftChild(i)], A[i] = A[i], A[leftChild(i)]
				if leftChild(leftChild(i)) < length:
					modifiedMaxHeapify(A,leftChild(i),length)

			if A[rightChild(i)] > A[i] and A[rightChild(i)] > A[leftChild(i)]:
				A[rightChild(i)], A[i] = A[i], A[rightChild(i)]
				if leftChild(rightChild(i)) < length:
					modifiedMaxHeapify(A,rightChild(i),length)

def modifiedMinHeapify(A,i,length):
	if leftChild(i) < length:
		if rightChild(i) >= length:
			# only left child exists
			if A[leftChild(i)] < A[i]:
				A[leftChild(i)], A[i] = A[i], A[leftChild(i)]
		else:
			# have both right and left
			if A[leftChild(i)] < A[i] and A[leftChild(i)] < A[rightChild(i)]:
				# swap left with i
				A[leftChild(i)], A[i] = A[i], A[leftChild(i)]
				if leftChild(leftChild(i)) < length:
					modifiedMinHeapify(A,leftChild(i),length)

			if A[rightChild(i)] < A[i] and A[rightChild(i)] < A[leftChild(i)]:
				A[rightChild(i)], A[i] = A[i], A[rightChild(i)]
				if leftChild(rightChild(i)) < length:
					modifiedMinHeapify(A,rightChild(i),length)

def extractMax(maxHeap):
	maxHeap[0] = maxHeap[-1]
	maxHeap.pop()
	modifiedMaxHeapify(maxHeap,0,len(maxHeap))

def extractMin(minHeap):
	minHeap[0] = minHeap[-1]
	minHeap.pop()
	modifiedMinHeapify(minHeap,0,len(minHeap))

def getMedian(maxHeap, minHeap):
	# THE FOLLOW IMPLEMENTATION IS DIFFERENT
	# WHICH TAKES AVERAGE WHEN TWO BUCKETS ARE OF SAME SIZE
	'''
	if len(maxHeap) == len(minHeap):
		return (maxHeap[0] + minHeap[0])/2
	else:
		if len(maxHeap) > len(minHeap):
			return maxHeap[0]
		else:
			return minHeap[0]
	'''
	maxHeapLength = len(maxHeap)
	minHeapLength = len(minHeap)
	totalLength = maxHeapLength + minHeapLength

	if totalLength % 2 == 0:
		# k/2 case
		return maxHeap[0]
	else:
		# (k+1)/2
		if maxHeapLength > minHeapLength:
			return maxHeap[0]
		else:
			return minHeap[0]

maxHeap = []
minHeap = []
median = []
#data = [1,2,3,4,5,6,7,8,9,10]
data = readData('median.txt')

for i in range(len(data)):
	addNumber(data[i], maxHeap, minHeap)
	rebalance(maxHeap, minHeap)
	median.append(getMedian(maxHeap, minHeap))

print(maxHeap)
print(minHeap)
print(median)
print(sum(median) % 10000)





