def readData(filename):
	file = open(filename)
	text = file.read()
	file.close()

	text = text.split('\n')

	dic = {}

	for i in range(len(text)):
		if int(text[i]) not in dic:
			dic[int(text[i])] = 1

	return sorted(list(dic))

def readDataToDic(filename):
	file = open(filename)
	text = file.read()
	file.close()

	text = text.split('\n')

	dic = {}

	for i in range(len(text)):
		if int(text[i]) not in dic:
			dic[int(text[i])] = 1

	return dic