def bubblesort(A):
	isSorted = False

	while(not isSorted):
		isSorted = True
		for i in range(len(A)-1):
			if A[i] > A[i+1]:
				A[i], A[i+1] = A[i+1], A[i]
				isSorted = False

if __name__ == '__main__':
	test = [3,5,4,2,6,7,1]
	bubblesort(test)
	print(test)