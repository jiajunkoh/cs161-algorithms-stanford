def selectionsort(A):
	l = len(A)
	for i in range(l):
		low = A[i]
		lowIndex = i
		for j in range(i+1,l):
			if low > A[j]:
				low = A[j]
				lowIndex = j
		A[i], A[lowIndex] = A[lowIndex], A[i]

if __name__ == '__main__':
	test = [1,3,5,2,4,8,7,6]
	selectionsort(test)
	print(test)