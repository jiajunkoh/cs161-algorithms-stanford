def readClustering(filename='clustering_big.txt'):
    file = open(filename)
    text = file.read()
    file.close()
    text = text.split('\n')
    
    node = []
    for i in range(1, len(text)-1):
        row = text[i].split(' ')
        bits = []
        for j in range(len(row)):
            try:
                bit = int(row[j])
                bits.append(bit)
            except Exception as e:
                pass
        node.append(bits)

    return node

def hammingDistance(a,b):
    hd = 0
    for i in range(len(a)):
        if a[i] != b[i]:
            hd = hd + 1
    return hd

nodes = readClustering()

# need to find the largest value of k, for spacing/hammingdistance = 3

