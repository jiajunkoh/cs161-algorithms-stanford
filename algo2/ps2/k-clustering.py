from readclustering import readClustering
from collections import OrderedDict

Eptr = readClustering()

sorted_Eptr = OrderedDict(sorted(Eptr.items(), key=lambda x:x[1]))

cluster_count = 500

vertices = {}
for edge in sorted_Eptr:
	edge = edge.split(',')
	vertices[int(edge[0])] = int(edge[0])
	vertices[int(edge[1])] = int(edge[1])

for edge in sorted_Eptr:
	edge = edge.split(',')
	edge[0] = int(edge[0])
	edge[1] = int(edge[1])

	head1 = vertices[edge[0]]
	while vertices[head1] != head1:
		head1 = vertices[head1]

	head2 = vertices[edge[1]]
	while vertices[head2] != head2:
		head2 = vertices[head2]

	if head1 != head2:
		if cluster_count <= 4:
			spacing_distance = sorted_Eptr[str(edge[0]) + ',' + str(edge[1])]
			print(spacing_distance)
			break
		vertices[head2] = head1
		cluster_count -= 1




			