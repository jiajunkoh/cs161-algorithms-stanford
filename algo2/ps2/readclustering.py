def readClustering(filename='clustering1.txt'):
	file = open(filename)
	text = file.read()
	file.close()

	text = text.split('\n')

	Eptr = {}
	for i in range(1,len(text)):
		line = text[i].split(' ')
		Eptr[line[0] + ',' + line[1]] = int(line[2])

	return Eptr

