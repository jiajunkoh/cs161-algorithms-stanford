def readjobs(filename='jobs.txt'):
	file = open(filename)
	text = file.read()
	file.close()

	text = text.split('\n')

	weights = []
	lengths = []
	for i in range(1,len(text)):
		line = text[i].split(' ')
		weights.append(int(line[0]))
		lengths.append(int(line[1]))

	return weights, lengths
		 

