def readEdges(filename='edges.txt'):
	file = open(filename)
	text = file.read()
	file.close()

	text = text.split('\n')

	E = {}
	for i in range(1,len(text)-1):	
		line = text[i].split(' ')
		if int(line[0]) > int(line[1]):
			a = int(line[1])
			b = int(line[0])
		else:
			a = int(line[0])
			b = int(line[1])

		key = str(a) + ',' + str(b)
		if key in E:
			if E[key] > int(line[2]):
				E[key] = int(line[2])
		else:
			E[key] = int(line[2])
	return E

shortestEdges = readEdges()
# select edge 1

X = [1]
# the non-selected edge
V = [i for i in range(1,501)]
T = 0
while len(X) != len(V):
	# let e = (u,v) be the cheapest edge of G
	# u in X, v not in X
	cheapestEdge = 99999999
	cheapestV = None
	cheapestKey = None
	for key in shortestEdges:
		splitKey = key.split(',')
		u = int(splitKey[0])
		v = int(splitKey[1])
		if u in X and v not in X and shortestEdges[key] < cheapestEdge:
			cheapestV = v
			cheapestKey = key
			cheapestEdge = shortestEdges[key]
		elif v in X and u not in X and shortestEdges[key] < cheapestEdge:
			cheapestV = u
			cheapestEdge = shortestEdges[key]
			cheapestKey = key

	print(len(X))
	T = T + cheapestEdge
	X.append(cheapestV)
print(X)
print(T)
























