from readjobs import readjobs

weights, lengths = readjobs()

import collections

ratio = {}
for i in range(len(weights)):
	r = weights[i]/lengths[i]
	if r in ratio:
		ratio[r].append(i)
	else:
		ratio[r] = [i]

ratio_od = collections.OrderedDict(sorted(ratio.items(), reverse=True))

weightedSum = 0
completionTime = 0

for k in ratio_od:
	for i in ratio_od[k]:
		completionTime = completionTime + lengths[i]
		weightedSum = weightedSum + completionTime*weights[i]

print(weightedSum)