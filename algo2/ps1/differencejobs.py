from readjobs import readjobs

weights, lengths = readjobs()

# compute min(sum(wj*cj))
import collections

# get difference
difference = {}
for i in range(len(weights)):
	diff = weights[i] - lengths[i]
	if diff in difference:
		difference[diff].append(i)
	else:
		difference[diff] = [i]

# decreasing value of wj-lj
difference_od = collections.OrderedDict(sorted(difference.items(), reverse=True))

weightedSum = 0
completionTime = 0

for k in difference_od:
	w = []
	l = []
	for i in difference_od[k]:
		# order by weights
		w.append(weights[i])
		l.append(lengths[i])
	w.sort(reverse=True)
	l.sort(reverse=True)
	for i in range(len(w)):
		completionTime = completionTime + l[i]
		weightedSum = weightedSum + completionTime*w[i]

print(weightedSum)
		











