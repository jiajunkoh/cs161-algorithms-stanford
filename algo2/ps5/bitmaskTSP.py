def readCityCoor(filename="tsp.txt"):
    file = open(filename)
    text = file.read()
    file.close()
    text = text.split('\n')
    coor = []
    for i in range(1,len(text)-1):
        line = text[i].split(' ')
        line[0] = float(line[0])
        line[1] = float(line[1])
        coor.append(line)
    return coor

coordinates = readCityCoor()

def EuclideanDistance(a,b):
    return ((a[0]-b[0])**2 + (a[1]-b[1])**2)**0.5

def numOfDigits(a):
    digits = 0
    while int(a) > 0:
        digits += 1
        a = a / 10
    return digits

def toBinary(x):
    reversedBinary = 0
    digit = 0
    while(x > 0):
        remainder = x % 2
        x = int(x/2)
        reversedBinary = reversedBinary + remainder*10**digit
        digit = digit + 1
    return reversedBinary

def bitmask(n=24):
    A = {}
    key = toBinary(0)
    keyLength = numOfDigits(key)
    key = '0'*(n) + '1'
    A[key] = 0
    for i in range(1,2**n):
        key = toBinary(i)
        keyLength = numOfDigits(key)
        key = '0'*(n-keyLength) + str(key) + '1'
        A[key] = 99999
    return A

A = bitmask()
destination = [toBinary(2**i) for i in range(1,25)]

# need one function to match the bit
def matchBits(dest, subset):
    '''
        dest: destination in binary
        subset: current iteration of subset
    '''
    strDest = str(dest)
    if subset[-1*len(strDest)] == '1':
        return True
    return False

# a fucking function to do A[s-{j}]
def minusFromSubset(s,j,n=25):
    '''
        s is string; j is int
        j must be in s
    '''
    result = int(s) - j
    resultLength = numOfDigits(result)
    result = '0'*(n-resultLength) + str(result)
    return result

# need to get m which is easy combination
import itertools
for m in range(1,25):
    print(m)
    subsets = tuple(itertools.combinations(destination,m))
    # need to edit subsets here
    for S in subsets: # for each set of size m
        s = 0
        for each in S:
            s = s + each
        s = s + 1
        sLength = numOfDigits(s)
        s = '0'*(25-sLength) + str(s)
        #print(s)
        for j in destination: 
            if matchBits(j,s): # for each j in s
                # A[s,j] = min(k in s, k !=j) A[s-j,k] + ckj
                minusSubsetKey = minusFromSubset(s,j)
                minVal = 999999
                if m == 1:
                    A[s] = A[minusSubsetKey] + EuclideanDistance(coordinates[0], coordinates[1 + destination.index(j)])
                else:
                    for k in destination:
                        if matchBits(k,s) and k!=j:
                            #print(minusSubsetKey)
                            #print(A[minusSubsetKey])
                            distance = A[minusSubsetKey] + EuclideanDistance(coordinates[1+destination.index(j)], coordinates[1+destination.index(k)])
                            #print(distance)
                            if distance < minVal:
                                minVal = distance
                    A[s] = minVal
minDist = 999999
finalSubset = '1'*25
for j in range(1,len(coordinates)):
    dist = A[finalSubset] + EuclideanDistance(coordinates[0], coordinates[j])
    print(dist)
    if dist < minDist:
        minDist = dist

print(minDist)





