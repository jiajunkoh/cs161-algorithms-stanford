def readCityCoor(filename="tsp.txt"):
    file = open(filename)
    text = file.read()
    file.close()
    text = text.split('\n')
    coor = []
    for i in range(1,len(text)-1):
        line = text[i].split(' ')
        line[0] = float(line[0])
        line[1] = float(line[1])
        coor.append(line)
    return coor

def EuclideanDistance(a,b):
    return ((a[0]-b[0])**2 + (a[1]-b[1])**2)**0.5


coordinates = readCityCoor()

tspDistance = 0

# starting = index = 0

remaining = [i for i in range(1,25)]
start = coordinates[0]
nextCoordinate = None
while len(remaining) > 0:
    # pick minimum distance
    minDistance = 99999
    minDistanceIndex = None
    for i in range(len(remaining)):
        distance = EuclideanDistance(start, coordinates[remaining[i]])
        if distance < minDistance:
            minDistance = distance
            nextCoordinate = coordinates[remaining[i]]
            minDistanceIndex = i
    print(coordinates.index(nextCoordinate))
    start = nextCoordinate
    del remaining[minDistanceIndex]
    tspDistance = tspDistance + minDistance

# need to go back to origin
tspDistance = tspDistance + EuclideanDistance(coordinates[0],nextCoordinate)
print(tspDistance)
