import numpy as np

def readCityCoor(filename="tsp.txt"):
    file = open(filename)
    text = file.read()
    file.close()
    text = text.split('\n')
    coor = []
    for i in range(1,len(text)-1):
        line = text[i].split(' ')
        line[0] = float(line[0])
        line[1] = float(line[1])
        coor.append(line)
    return coor

def EuclideanDistance(a,b):
    return ((a[0]-b[0])**2 + (a[1]-b[1])**2)**0.5

coordinates = readCityCoor()
destination = [i for i in range(1,25)]

# to get 2**n * 2 array is not really 'practical'
# subset
import itertools
def getAllSubsets(dest):
    allSubsets = []
    for i in range(len(dest)+1):
        iItemsSubsets = list(itertools.combinations(dest,i))
        for j in range(len(iItemsSubsets)):
            allSubsets.append(tuple([0] + list(iItemsSubsets[j])))
    return allSubsets

allSubsets = getAllSubsets(destination)
A = {}
print(len(allSubsets))
for i in range(len(allSubsets)):
    print('getting subsets @ {0}'.format(i))
    for j in range(25):
        if j == 0 and len(allSubsets[i]) == 1:
            A[(allSubsets[i], j)] = 0
        else:
            A[(allSubsets[i], j)] = 999999

for i in range(len(allSubsets)):
    print('calculating subsets @ {0}'.format(i))
    for j in range(len(destination)):
        if destination[j] in allSubsets(i):
            minVal = 999999
            for k in range(len(destination)):
                if destination[k] in allSubsets[i] and destination[k] != destination[j]:
                    # need to slice of {destination[j]}
                    previous_subset = []
                    for k in range(allSubsets[i]):
                        if allSubsets[i][k] != destination[j]:
                            previous_subset.append(allSubsets[i][k])
                    dist = A[(previous_subset, destination[k])] + EuclideanDistance(coordinates[destination[k]], coordinates[destination[j]])
                    if dist < minval:
                        minVal = dist
            A[(allSubsets[i],j)] = minVal

minDist = 999999
finalSubset = (i for i in range(25))
for j in range(len(destination)):
    dist = A[(finalSubset, j)] + EuclideanDistance(coordinates[destination[j]], coordinates[0])
    if dist < minDist:
        minDist = dist

print(minDist)

