def readFile(filename):
	file = open(filename)
	text = file.read()
	file.close()

	text = text.split('\n')

	line = text[0].split(' ')
	W = int(line[0])

	val = []
	weight = []
	print('reading data')
	for i in range(1,len(text)-1):
		print(i)
		line = text[i].split(' ')
		val.append(int(line[0]))
		weight.append(int(line[1]))
	print('done reading data')
	return W, val, weight
'''
W, value, weight = readFile('knapsack1.txt')
numItems = len(value)

A = []
B = []
for i in range(W+1):
	B.append(0)

for i in range(2):
	A.append([])
	A[i] = B.copy() 

for i in range(1, numItems+1):
	print(i)
	for x in range(W + 1):
		if x - weight[i-1] >= 0 and x - weight[i-1] <= W:
			A[1][x] = max([A[0][x], A[0][x-weight[i-1]] + value[i-1]])
		else:
			A[1][x] = A[0][x]
	A[0] = A[1].copy()

print(A[1][W])
'''

W, value, weight = readFile('knapsack_big.txt')
numItems = len(value)

A = []
B = []
for i in range(W + 1):
	B.append(0)

for i in range(2):
	A.append([])
	A[i] = B.copy() 

for i in range(1, numItems+1):
	print(i)
	for x in range(8074, W + 1):
		if x - weight[i-1] >= 0 and x - weight[i-1] <= W:
			A[1][x] = max([A[0][x], A[0][x-weight[i-1]] + value[i-1]])
		else:
			A[1][x] = A[0][x]
	A[0] = A[1].copy()

print(A[1][W])






