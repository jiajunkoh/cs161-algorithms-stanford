# reduce ASPS to
# 1 invocation of Bellman-Ford O(mn)
# n invocation of Dijkstra O(mnlogn)

def readGraph(filename):
    file = open(filename)
    text = file.read()
    file.close()

    text = text.split('\n')
    Eptr = {}
    E = []
    adj = {}
    for i in range(1, len(text)-1):
        line = text[i].split(' ')
        Eptr[line[0] + ',' + line[1]] = int(line[2])
        E.append([line[0], line[1]])
        if line[0] in adj:
            adj[line[0]].append(line[1])
        else:
            adj[line[0]] = [line[1]]
    return E, Eptr, adj

# after running BF on all 3 graphs, only g3.txt no negative cycle
E, Eptr, adj = readGraph(filename='g3.txt')

shortestDistance = {}
V = [str(i) for i in range(1,1001)]

# form G' by adding a new vertex s and a new edge (s,v) with length 0 for each v in G
Vori = V.copy()
Eori = E.copy()
Eptrori = Eptr.copy()
V.append('s')
for i in range(len(V)-1):
    E.append(['s', V[i]])
    Eptr['s,' + V[i]] = 0

#print(Eptr)
#print(E)

# run bf with source = s
def bellmanFord(s, V, E, WEIGHT):
    for i in V:
        if i == 's':
            shortestDistance[i] = 0
        else:
            shortestDistance[i] = 999999
    
    for i in range(len(V) - 1):
        print(i)
        for edge in E:
            u = edge[0]
            v = edge[1]
            if shortestDistance[v] > shortestDistance[u] + WEIGHT[u + ',' + v]:
                shortestDistance[v] = shortestDistance[u] + WEIGHT[u + ',' + v]   

    for edge in E:
        u = edge[0]
        v = edge[1]
        if shortestDistance[v] > shortestDistance[u] + WEIGHT[u + ',' + v]:
            print('NEGATIVE WEIGHT CYCLE EXISTS!')
            return

bellmanFord('s', V, E, Eptr)
#print(shortestDistance)

# reweighting factor
# note that pv is actually same as shortestDistance
# ce' = ce + pu - pv

ce_prime = {}
for edge in E:
    u = edge[0]
    v = edge[1]
    ce_prime[u + ',' + v] = Eptr[u + ',' + v] + shortestDistance[u] - shortestDistance[v]

def dijkstra(s, V, E, Eptr):
    # initialise
    X = [s]
    # computed shortest path distance
    A = {}
    A[s] = 0
    # computed shortest path
    B = {}
    B[s] = []
    
    # main loop
    while len(X) != len(V)-1:
        vstar = None
        wstar = None
        dijkstra_greedy_criterion = 999999
        for edge in E:
            v = edge[0]
            w = edge[1]
            if v in X and w not in X:
                if A[v] + Eptr[v + ',' + w] < dijkstra_greedy_criterion:
                    dijkstra_greedy_criterion = A[v] + Eptr[v + ',' + w]
                    vstar = v
                    wstar = w
        X.append(wstar)
        A[wstar] = A[vstar] + Eptr[vstar + ',' + wstar]
        B[wstar] = B[vstar].copy()
        B[wstar].append(vstar)
        B[wstar].append(wstar)
    return A

# need to use heap for dijkstra


d_prime_shortestPath = {}
for u in V:
    print(u)
    if u != 's':
       d_prime_shortestPath[u] = {}
       d_prime_shortestPath[u] = dijkstra(u, Vori, Eori, ce_prime)

apsp = {}
for u in V:
    for v in V:
        print('({u}, {v})'.format(u,v))
        if u != v:
            apsp[u + ',' + v] = d_prime_shortestPath[u][v] - shortestDistance[u] + shortestDistance[v] 

print(min(apsp, key=apsp.get))

