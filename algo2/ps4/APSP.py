import numpy as np

def readGraph(filename):
	file = open(filename)
	text = file.read()
	file.close()

	text = text.split('\n')
	Eptr = {}
	for i in range(1,len(text)-1):
		line = text[i].split(' ')
		Eptr[line[0] + ',' + line[1]] = int(line[2])
	return Eptr


def FloydWarshall(Eptr):
	A = np.zeros([1000,1000,1001])

	# base case
	for i in range(1000):
		for j in range(1000):
			if i == j:
				A[i,j,0] = 0
			elif str(i+1) + ',' + str(j+1) in Eptr:
				A[i,j,0] = Eptr[str(i+1) + ',' + str(j+1)]
			else:
				A[i,j,0] = 999999

	minVal = 999999
	for k in range(1,1001):
		print(k)
		for i in range(1000):
			for j in range(1000):
				A[i,j,k] = min(A[i,j,k-1], A[i,k,k-1] + A[k,j,k-1])
				if A[i,j,k] < minVal:
					minVal = A[i,j,k]

Eptr1 = readGraph('g1.txt')
print(FloydWarshall(Eptr1))
#Eptr1 = readGraph('g2.txt')
#Eptr1 = readGraph('g3.txt')
