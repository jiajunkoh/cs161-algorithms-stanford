import numpy as np
def readGraph(filename):
    file = open(filename)
    text = file.read()
    file.close()

    text = text.split('\n')
    Eptr = {}
    for i in range(1, len(text) - 1):
        line = text[i].split(' ')
        Eptr[line[0] + ',' + line[1]] = int(line[2])
    return Eptr


def FloydWarshall(Eptr):
    A = np.zeros([1001,1001,1001])

    # base case
    for i in range(1,1001):
        for j in range(1,1001):
            if i == j:
                A[i,j,0] = 0
            elif str(i) + ',' + str(j) in Eptr:
                A[i,j,0] = Eptr[str(i) + ',' + str(j)]
            else:
                A[i,j,0] = 999999

    # iteration
    for k in range(1,1001):
        print('k={k}'.format(k=k))
        for i in range(1,1001):
            for j in range(1,1001):
                A[i,j,k] = min(A[i,j,k-1], A[i,k,k-1]+A[k,j,k-1])

    shortestPath = 0

    for i in range(1,1001):
        for j in range(1,1001):
            val = A[i,j,1000]
            if val < shortestPath:
                shortestPath = val
    return shortestPath

Eptr = readGraph('g3.txt')
print(FloydWarshall(Eptr))

